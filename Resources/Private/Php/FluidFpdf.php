<?php

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

class FluidFpdf extends \setasign\Fpdi\FPDI
{

    /**
     * Current font spacing in points
     * @var float
     */
    protected $fontSpacingPt;

    /**
     * Current font spacing in user units
     * @var float
     */
    protected $fontSpacing;


    /**
     * EXT:Bookmarks: outline
     * @var array
     */
    protected $outlines = [];

    /**
     * EXT:Bookmarks: outline root
     * @var string
     */
    protected $outlineRoot;

    /**
     * EXT:Footer
     * @var string
     */
    protected $footerTemplate;

    /**
     * EXT:Footer
     * @var array
     */
    protected $footerArguments;

    /**
     * EXT:Bookmarks
     * @param $txt
     * @param bool $isUTF8
     * @param int $level
     * @param int $y
     */
    function Bookmark($txt, $isUTF8 = false, $level = 0, $y = 0)
    {
        if (!$isUTF8) {
            $txt = utf8_encode($txt);
        }
        if ($y == -1) {
            $y = $this->GetY();
        }
        $this->outlines[] = ['t' => $txt, 'l' => $level, 'y' => ($this->h - $y) * $this->k, 'p' => $this->PageNo()];
    }

    /**
     * EXT:Bookmarks
     */
    function _putbookmarks()
    {
        $nb = count($this->outlines);
        if ($nb == 0) {
            return;
        }
        $lru = [];
        $level = 0;
        foreach ($this->outlines as $i => $o) {
            if ($o['l'] > 0) {
                $parent = $lru[$o['l'] - 1];
                // Set parent and last pointers
                $this->outlines[$i]['parent'] = $parent;
                $this->outlines[$parent]['last'] = $i;
                if ($o['l'] > $level) {
                    // Level increasing: set first pointer
                    $this->outlines[$parent]['first'] = $i;
                }
            } else {
                $this->outlines[$i]['parent'] = $nb;
            }
            if ($o['l'] <= $level && $i > 0) {
                // Set prev and next pointers
                $prev = $lru[$o['l']];
                $this->outlines[$prev]['next'] = $i;
                $this->outlines[$i]['prev'] = $prev;
            }
            $lru[$o['l']] = $i;
            $level = $o['l'];
        }
        // Outline items
        $n = $this->n + 1;
        foreach ($this->outlines as $i => $o) {
            $this->_newobj();
            $this->_put('<</Title ' . $this->_textstring($o['t']));
            $this->_put('/Parent ' . ($n + $o['parent']) . ' 0 R');
            if (isset($o['prev'])) {
                $this->_put('/Prev ' . ($n + $o['prev']) . ' 0 R');
            }
            if (isset($o['next'])) {
                $this->_put('/Next ' . ($n + $o['next']) . ' 0 R');
            }
            if (isset($o['first'])) {
                $this->_put('/First ' . ($n + $o['first']) . ' 0 R');
            }
            if (isset($o['last'])) {
                $this->_put('/Last ' . ($n + $o['last']) . ' 0 R');
            }
            $this->_put(sprintf('/Dest [%d 0 R /XYZ 0 %.2F null]', $this->PageInfo[$o['p']]['n'], $o['y']));
            $this->_put('/Count 0>>');
            $this->_put('endobj');
        }
        // Outline root
        $this->_newobj();
        $this->outlineRoot = $this->n;
        $this->_put('<</Type /Outlines /First ' . $n . ' 0 R');
        $this->_put('/Last ' . ($n + $lru[0]) . ' 0 R>>');
        $this->_put('endobj');
    }

    /**
     * EXT:Bookmarks
     */
    function _putresources()
    {
        parent::_putresources();
        $this->_putbookmarks();
    }

    /**
     * EXT:Bookmarks
     */
    function _putcatalog()
    {
        parent::_putcatalog();
        if (count($this->outlines) > 0) {
            $this->_put('/Outlines ' . $this->outlineRoot . ' 0 R');
            $this->_put('/PageMode /UseOutlines');
        }
    }

    /**
     * EXT:Index
     * @param string $pagePrefix
     */
    function CreateIndex($pagePrefix='p. ')
    {
        $size = sizeof($this->outlines);
        // Width of the highest/last page
        $PageCellSize = $this->GetStringWidth($pagePrefix . $this->outlines[$size - 1]['p']) + 2;
        // Foreach index entry
        for ($i = 0; $i < $size; $i++) {
            //Offset
            $level = $this->outlines[$i]['l'];
            if ($level > 0) {
                $this->Cell($level * 8);
            }
            //Caption
            $str = utf8_decode($this->outlines[$i]['t']);
            $strsize = $this->GetStringWidth($str);
            $avail_size = $this->w - $this->lMargin - $this->rMargin - $PageCellSize - ($level * 8) - 4;
            while ($strsize >= $avail_size) {
                $str = substr($str, 0, -1);
                $strsize = $this->GetStringWidth($str);
            }
            $this->Cell($strsize + 2, $this->FontSize + 2, $str);
            //Filling dots
            $w = $this->w - $this->lMargin - $this->rMargin - $PageCellSize - ($level * 8) - ($strsize + 2);
            $nb = $w / $this->GetStringWidth('.');
            $dots = str_repeat('.', $nb);
            $this->Cell($w, $this->FontSize + 2, $dots, 0, 0, 'R');
            //Page number
            $this->Cell($PageCellSize, $this->FontSize + 2, $pagePrefix . $this->outlines[$i]['p'], 0, 1, 'R');
        }
    }

    /**
     * EXT:Index
     * @param int $level The level of index entries. -1 means all levels.
     * @return array
     */
    function GetIndex($level=-1) {
        $entries = [];
        if(count($this->outlines) > 0) {
            foreach($this->outlines as $outline) {
                if($level === $outline['l']) {
                    $entries[] = $outline;
                }
                else if($level === -1) {
                    $entries[] = $outline;
                }
            }
        }
        return $entries;
    }

    /**
     * @param int $number Number of the page. -1 means last page.
     */
    function GoToPage($number=-1)
    {
        if($number === -1) {
            $number = count($this->pages);
        }
        $this->page = $number;
    }

    /**
     * EXT:SetFontSpacing
     * @param float $spacing
     */
    function SetFontSpacing($spacing=0.0)
    {
        if ($this->fontSpacingPt == $spacing) {
            return;
        }
        $this->fontSpacingPt = $spacing;
        $this->fontSpacing = $spacing / $this->k;
        if ($this->page > 0) {
            $this->_out(sprintf('BT %.3f Tc ET', $spacing));
        }
    }

    /**
     * EXT:EPS
     * @param $file
     * @param $x
     * @param $y
     * @param int $w
     * @param int $h
     * @param string $link
     * @param bool $useBoundingBox
     * @return bool
     * @throws Exception
     */
    function ImageEps ($file, $x, $y, $w=0, $h=0, $link='', $useBoundingBox=true){
        $data = file_get_contents($file);
        if ($data===false) {
            $this->Error('EPS file not found: '.$file);
        }
        $regs = array();
        # EPS/AI compatibility check (only checks files created by Adobe Illustrator!)
        preg_match ('/%%Creator:([^\r\n]+)/', $data, $regs); # find Creator
        if (count($regs)>1){
            $version_str = trim($regs[1]); # e.g. "Adobe Illustrator(R) 8.0"
            if (strpos($version_str, 'Adobe Illustrator')!==false){
                $a = explode(' ', $version_str);
                $version = (float)array_pop($a);
                if ($version>=9)
                    $this->Error('File was saved with wrong Illustrator version: '.$file);
                #return false; # wrong version, only 1.x, 3.x or 8.x are supported
            }#else
            #$this->Error('EPS wasn\'t created with Illustrator: '.$file);
        }
        # strip binary bytes in front of PS-header
        $start = strpos($data, '%!PS-Adobe');
        if ($start>0) $data = substr($data, $start);
        # find BoundingBox params
        preg_match ("/%%BoundingBox:([^\r\n]+)/", $data, $regs);
        if (count($regs)>1){
            list($x1,$y1,$x2,$y2) = explode(' ', trim($regs[1]));
        }
        else {
            $this->Error('No BoundingBox found in EPS file: '.$file);
        }
        $start = strpos($data, '%%EndSetup');
        if ($start===false) $start = strpos($data, '%%EndProlog');
        if ($start===false) $start = strpos($data, '%%BoundingBox');
        $data = substr($data, $start);
        $end = strpos($data, '%%PageTrailer');
        if ($end===false) $end = strpos($data, 'showpage');
        if ($end) $data = substr($data, 0, $end);
        # save the current graphic state
        $this->_out('q');
        $k = $this->k;
        if ($useBoundingBox){
            $dx = $x*$k-$x1;
            $dy = $y*$k-$y1;
        }else{
            $dx = $x*$k;
            $dy = $y*$k;
        }
        # translate
        $this->_out(sprintf('%.3F %.3F %.3F %.3F %.3F %.3F cm', 1,0,0,1,$dx,$dy+($this->hPt - 2*$y*$k - ($y2-$y1))));
        if ($w>0){
            $scale_x = $w/(($x2-$x1)/$k);
            if ($h>0){
                $scale_y = $h/(($y2-$y1)/$k);
            }else{
                $scale_y = $scale_x;
                $h = ($y2-$y1)/$k * $scale_y;
            }
        }else{
            if ($h>0){
                $scale_y = $h/(($y2-$y1)/$k);
                $scale_x = $scale_y;
                $w = ($x2-$x1)/$k * $scale_x;
            }else{
                $w = ($x2-$x1)/$k;
                $h = ($y2-$y1)/$k;
            }
        }
        # scale
        if (isset($scale_x)) {
            $this->_out(sprintf('%.3F %.3F %.3F %.3F %.3F %.3F cm', $scale_x, 0, 0, $scale_y, $x1 * (1 - $scale_x),
                $y2 * (1 - $scale_y)));
        }
        # handle pc/unix/mac line endings
        $lines = preg_split ("/\r\n|[\r\n]/", $data);
        $u = 0;
        $cnt = count($lines);
        for ($i=0;$i<$cnt;$i++){
            $line = $lines[$i];
            if ($line=='' || $line[0]=='%') continue;
            $len = strlen($line);
            $chunks = explode(' ', $line);
            $cmd = array_pop($chunks);
            # RGB
            if ($cmd=='Xa'||$cmd=='XA'){
                $b = array_pop($chunks); $g = array_pop($chunks); $r = array_pop($chunks);
                $this->_out("$r $g $b ". ($cmd=='Xa'?'rg':'RG') ); #substr($line, 0, -2).'rg' -> in EPS (AI8): c m y k r g b rg!
                continue;
            }
            switch ($cmd){
                case 'm':
                case 'l':
                //case 'v':
                case 'y':
                case 'c':
                case 'k':
                case 'K':
                case 'g':
                case 'G':
                case 's':
                case 'S':
                case 'J':
                case 'j':
                case 'w':
                case 'M':
                case 'd' :
                case 'n' :
                case 'v' :
                    $this->_out($line);
                    break;
                case 'x': # custom fill color
                    list($c,$m,$y,$k) = $chunks;
                    $this->_out("$c $m $y $k k");
                    break;
                case 'X': # custom stroke color
                    list($c,$m,$y,$k) = $chunks;
                    $this->_out("$c $m $y $k K");
                    break;
                case 'Y':
                case 'N':
                case 'V':
                case 'L':
                case 'C':
                    $line[$len-1] = strtolower($cmd);
                    $this->_out($line);
                    break;
                case 'b':
                case 'B':
                    $this->_out($cmd . '*');
                    break;
                case 'f':
                case 'F':
                    if ($u>0){
                        $isU = false;
                        $max = min($i+5,$cnt);
                        for ($j=$i+1;$j<$max;$j++)
                            $isU = ($isU || ($lines[$j]=='U' || $lines[$j]=='*U'));
                        if ($isU) $this->_out("f*");
                    }else
                        $this->_out("f*");
                    break;
                case '*u':
                    $u++;
                    break;
                case '*U':
                    $u--;
                    break;
                #default: echo "$cmd<br>"; #just for debugging
            }
        }
        # restore previous graphic state
        $this->_out('Q');
        if ($link)
            $this->Link($x,$y,$w,$h,$link);
        return true;
    }

    function Header()
    {
        // To be implemented in your own inherited class
        if ($this->_tpls[2]) {
            $this->useTemplate(2, 0, 0);
        } else {
            if ($this->_tpls[1]) {
                $this->useTemplate(1, 0, 0);
            } else {
                if ($this->_tpls[0]) {
                    $this->useTemplate(0, 0, 0);
                }
            }
        }
    }

    /**
     * @throws \TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotException
     * @throws \TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotReturnException
     */
    function Footer()
    {
        $this->_getDispatcher()->dispatch(__CLASS__, 'footer', [$this]);
    }

    function SetFooterTemplate($footerTemplate) {
        $this->footerTemplate = $footerTemplate;
    }

    function GetFooterTemplate() {
        return $this->footerTemplate;
    }

    function SetFooterArguments($footerArguments) {
        $this->footerArguments = $footerArguments;
    }

    function GetFooterArguments() {
        return $this->footerArguments;
    }

    /**
     * @return Dispatcher
     */
    protected function _getDispatcher()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var Dispatcher $dispatcher */
        $dispatcher = $objectManager->get(Dispatcher::class);
        return $dispatcher;
    }


    function GetDefaultPageSize() {
        return $this->DefPageSize;
    }
    function GetCurrentPageSize() {
        return $this->CurPageSize;
    }
    function GetDefaultOrientation() {
        return $this->DefOrientation;
    }
    function GetCurrentOrientation() {
        return $this->CurOrientation;
    }


    /**
     * Settings of HTML parser
     * @param array
     */
    protected $htmlSettings = [
        'lineHeight' => 5.0,
        'paragraphMarginBottom' => 7.5,
        'href' => '',
        'list' => [],
        'listLi' => false,
        'listIndent' => 8.0,
        'originalMarginLeft' => 0,
        'originalFontSize' => 0,
    ];

    /**
     * Source: http://fpdf.org/en/script/script42.php
     *
     * @param $html string HTML text
     * @param $lineHeight float Line height
     * @param $listIndent float List indent
     */

    /**
     * @param string $html HTML markup
     * @param int $x
     * @param int $width
     * @param float $lineHeight Line height
     * @param float $listIndent List indent
     * @param int $paragraphMarginBottom Margin bottom for paragraphs, listings and enumerations.
     */
    function WriteHTML($html, $x=30, $width=100, $lineHeight=5.0, $listIndent=8.0, $paragraphMarginBottom=0)
    {
        //
        // Save Margin left, so that we can restore it later
        $this->htmlSettings['originalMarginLeft'] = $this->lMargin;
        $this->htmlSettings['originalMarginRight'] = $this->rMargin;
        $this->lMargin = $x;
        $this->rMargin = $this->GetPageWidth() - ($x + $width);
        //
        // Prepare HTML settings
        $this->htmlSettings['lineHeight'] = $lineHeight;
        $this->htmlSettings['listIndent'] = $listIndent;
        $this->htmlSettings['paragraphMarginBottom'] = $paragraphMarginBottom;
        $this->htmlSettings['originalFontSize'] = $this->FontSizePt;
        //
        // Strip restricted tags and explode HTML
        $html = strip_tags($html, "<b><u><i><a><p><br><strike><strong><small><em><ul><ol><li>");
        //$html = str_replace("\n", ' ', $html);
        $html = str_replace('</ul> ', '</ul>', $html);
        $html = str_replace('</ol> ', '</ol>', $html);
        $html = str_replace('</li> ', '</li>', $html);
        $htmlParts = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        //
        // Set position to previous Line
        $this->SetXY($this->lMargin, ($this->GetY() - $this->htmlSettings['lineHeight']));
        //
        // Process HTML
        foreach ($htmlParts as $index => $htmlPart) {
            //
            // Check if we need a page break
            if($this->GetY()+20 > $this->PageBreakTrigger) {
                $this->AddPage();
            }
            //
            if ($index % 2 == 0) {
                // Text
                if (trim($this->htmlSettings['href']) !== '') {
                    $this->PutLink($this->htmlSettings['href'], $htmlPart);
                } else {
                    //
                    // In ul/ol list?
                    if(count($this->htmlSettings['list']) > 0 && $this->htmlSettings['listLi']) {
                        $x = $this->GetX();
                        $y = $this->GetY();
                        $listCharacter = '';
                        $listCharacterX = ($x - $this->htmlSettings['listIndent']);
                        $restoreFont = $this->FontFamily;
                        $restoreStyle = $this->FontStyle;
                        $currentList = $this->htmlSettings['list'][(count($this->htmlSettings['list'])-1)];
                        if($currentList['type'] === 'UL') {
                            $listCharacter = chr(127);
                            // Switch font to ensure, that the bullet is available
                            $this->SetFont('Courier', '');
                            $listCharacterX = $listCharacterX + ($this->htmlSettings['listIndent'] / 2);
                        }
                        else if($currentList['type'] === 'OL') {
                            if($currentList['bullets'] === '1') {
                                $listCharacter = $currentList['items'] . '.';
                                // Concatenated list numbers
                                //foreach($this->htmlSettings['list'] as $listLevel) {
                                //    $listCharacter .= $listLevel['items'] . '.';
                                //}
                            }
                            else if($currentList['bullets'] === 'a') {
                                $map = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'];
                                $listCharacter = $map[($currentList['items'])-1] . '.';
                            }
                        }
                        $this->htmlSettings['listLi'] = false;
                        //
                        // Check if we need a page break
                        if($this->GetY()+20 > $this->PageBreakTrigger) {
                            $this->AddPage();
                        }
                        //
                        // Print the List bullet or enumeration number
                        $this->SetXY($listCharacterX, $y);
                        $this->Write($this->htmlSettings['lineHeight'], $listCharacter);
                        $this->SetXY($x, $y);
                        // Restore font and style
                        if($currentList['type'] === 'UL') {
                            $this->SetFont($restoreFont, $restoreStyle);
                        }
                    }
                    //
                    // Remove tabs and newlines, which are leftovers from HTML structure
                    $writeText = stripslashes($this->TextEntities($htmlPart));
                    $writeText = str_replace("\t", '', $writeText);
                    $writeText = str_replace("\n", '', $writeText);
                    //
                    // Print normal Text
                    if($writeText !== '') {
                        $this->Write($this->htmlSettings['lineHeight'], $writeText);
                    }
                }
            } else {
                // Tag
                if ($htmlPart[0] === '/') {
                    $this->CloseTag(strtoupper(substr($htmlPart, 1)));
                } else {
                    // Extract attributes
                    $a2 = explode(' ', $htmlPart);
                    $tag = strtoupper(array_shift($a2));
                    $attr = [];
                    foreach ($a2 as $v) {
                        if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3)) {
                            $attr[strtoupper($a3[1])] = $a3[2];
                        }
                    }
                    $this->OpenTag($tag, $attr);
                }
            }
        }
        //
        // Restore left and right margin
        $this->SetLeftMargin($this->htmlSettings['originalMarginLeft']);
        $this->SetRightMargin($this->htmlSettings['originalMarginRight']);
        //
        // Set position to new Line
        $this->SetXY($this->lMargin, ($this->GetY() + $this->htmlSettings['lineHeight']));
    }

    /**
     * Processing HTML opening tag.
     * @param $tag string Tag identifier
     * @param $attr array Tag attributes
     */
    function OpenTag($tag, $attr)
    {
        //Opening tag
        switch ($tag) {
            case 'SMALL':
                $this->SetFontSize($this->FontSizePt * 0.8);
                break;
            case 'STRONG':
                $this->SetStyle('B', true);
                break;
            case 'EM':
                $this->SetStyle('I', true);
                break;
            case 'B':
            case 'I':
            case 'U':
                $this->SetStyle($tag, true);
                break;
            case 'A':
                $this->htmlSettings['href'] = $attr['HREF'];
                break;
            case 'TR':
            case 'BLOCKQUOTE':
            case 'BR':
                $this->Ln($this->htmlSettings['lineHeight']);
                break;
            case 'P':
                $this->Ln($this->htmlSettings['lineHeight'] + $this->htmlSettings['paragraphMarginBottom']);
                break;
            case 'UL':
                $this->lMargin += $this->htmlSettings['listIndent'];
                $this->htmlSettings['list'][count($this->htmlSettings['list'])] = [
                    'type' => 'UL',
                    'items' => 0,
                    'bullets' => isset($attr['TYPE']) ? trim($attr['TYPE']) : '1',
                ];
                break;
            case 'OL':
                $this->htmlSettings['list'][count($this->htmlSettings['list'])] = [
                    'type' => 'OL',
                    'items' => 0,
                    'bullets' => isset($attr['TYPE']) ? trim($attr['TYPE']) : '1',
                ];
                $this->lMargin += $this->htmlSettings['listIndent'];
                break;
            case 'LI':
                $this->htmlSettings['listLi'] = true;
                $this->htmlSettings['list'][(count($this->htmlSettings['list'])-1)]['items']++;
                $this->Ln($this->htmlSettings['lineHeight']);
                break;
        }
    }

    /**
     * Processing HTML closing tag.
     * @param $tag string Tag identifier
     */
    function CloseTag($tag)
    {
        if ($tag == 'SMALL') {
            $this->FontSizePt = $this->htmlSettings['originalFontSize'];
        }
        if ($tag == 'STRONG') {
            $tag = 'B';
        }
        if ($tag == 'EM') {
            $tag = 'I';
        }
        if ($tag == 'B' || $tag == 'I' || $tag == 'U') {
            $this->SetStyle($tag, false);
        }
        if ($tag === 'A') {
            $this->htmlSettings['href'] = '';
        }
        if ($tag === 'LI') {
            $y = $this->GetY();
            $y += $this->htmlSettings['paragraphMarginBottom'];
            $this->SetXY($this->lMargin, $y);
        }
        if ($tag === 'UL') {
            $this->lMargin -= $this->htmlSettings['listIndent'];
            unset($this->htmlSettings['list'][(count($this->htmlSettings['list'])-1)]);
            $y = $this->GetY();
            if(count($this->htmlSettings['list']) > 0) {
                // If we're still in a listing, add some space below
                $y += $this->htmlSettings['paragraphMarginBottom'];
            }
            $this->SetXY($this->lMargin, $y);
        }
        if ($tag === 'OL') {
            $this->lMargin -= $this->htmlSettings['listIndent'];
            unset($this->htmlSettings['list'][(count($this->htmlSettings['list'])-1)]);
            $y = $this->GetY();
            if(count($this->htmlSettings['list']) > 0) {
                // If we're still in a listing, add some space below
                $y += $this->htmlSettings['paragraphMarginBottom'];
            }
            $this->SetXY($this->lMargin, $y);
        }
    }

    function SetStyle($tag, $enable)
    {
        //Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach (['B', 'I', 'U'] as $s) {
            if ($this->$s > 0) {
                $style .= $s;
            }
        }
        $this->SetFont('', $style);
    }

    /**
     * Insert a link
     * @param $url
     * @param $text
     */
    protected function PutLink($url, $text)
    {
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $text, $url);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    }

    protected function TextEntities($html) {
        $trans = get_html_translation_table(HTML_ENTITIES);
        $trans = array_flip($trans);
        return strtr($html, $trans);
    }

}
