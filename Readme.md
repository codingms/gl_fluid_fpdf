# Fluid-FPDF for TYPO3
This extension provides you a complete set of ViewHelpers for dealing with FPDF by using Fluid. You can use the 
ViewHelpers easily in own Extensions just by defining the fpdf-Namespace. 
Additionally you're able to use the build in plugin, for displaying predefined PDF. This plugin offers you automatically
all available PDFs.

Features:
*	Fluid-ViewHelper for all FPDF-Functions
*	Usable in own Extensions
*	Predefine PDFs by using Fluid-Templates and provide them to your page-editors


## ViewHelpers


------------------------------------------------------------------------------------------------------------------------


### fpdf:addFont
Imports a TrueType, OpenType or Type1 font and makes it available.
It is necessary to generate a font definition file first with the MakeFont utility.
The definition file (and the font file itself when embedding) must be present in the font directory. If it is not found,
the error "Could not include font definition file" is raised.

#### Parameter

| **Name**     | **Type** | **Default** | **Description**                                                                                                            |
|:-------------|:---------|:------------|:---------------------------------------------------------------------------------------------------------------------------|
| **family**   | string   |             | Font family. The name can be chosen arbitrarily. If it is a standard family name, it will override the corresponding font. |
| **style**    | string   |             | Font style. (N: regular, B: bold, I: italic)                                                                               |
| **filename** | string   |             | The font definition file. By default, the name is built from the family and style, in lower case with no space.            |

#### Return
`void`

#### Usage:
```xml
<fpdf:setFont family="Arial" style="B" size="10.5" />
```

------------------------------------------------------------------------------------------------------------------------


### fpdf:pdf
Creates a new PDF.

#### Parameter

| **Name**         | **Type** | **Default**                             | **Description**                                                                                   |
|:-----------------|:---------|:----------------------------------------|:--------------------------------------------------------------------------------------------------|
| **filename**     | string   | FluidFpdf.pdf                           | The name of the file                                                                              |
| **destination**  | string   | I                                       | Destination where to send the document. It can take one of the following values: I, D, F, S       |
| **orientation**  | string   | P                                       | Default page orientation.                                                                         |
| **unit**         | string   | mm                                      | User unit.                                                                                        |
| **size**         | string   | A4                                      | The size used for pages.                                                                          |
| **title**        | string   | Fluid-Fpdf                              | Defines the title of the document.                                                                |
| **subject**      | string   | Fluid-Fpdf                              | Defines the subject of the document.                                                              |
| **author**       | string   | Fluid-Fpdf                              | FDefines the author of the document.                                                              |
| **keywords**     | string   | TYPO3 Fluid FPDF typo3@coding.ms        | Associates keywords with the document, generally in the form 'keyword1 keyword2 ...'.             |
| **fontPath**     | string   | EXT:fluid_fpdf/Resources/Private/Fonts  | Defines the font path for the included fonts (for example: EXT:fluid_fpdf/Resources/Private/Fonts) or some TYPO3 path like /fileadmin/fonts/    |
| **imagePath**    | string   | EXT:fluid_fpdf/Resources/Private/Images | Defines the image path for the included images (for example: EXT:fluid_fpdf/Resources/Private/Images) or some TYPO3 path like /fileadmin/fonts/ |
| **characterMap** | array    |                                         | Defines a character map, for mapping characters. For Example, for mapping €-character with character at ASCII-No. 162, use {162: '€'}           |

#### Return
`void`

#### Usage:
```xml
<fpdf:pdf>
	<fpdf:addPage orientation="P" size="A4">
		// Your PDF content...
	</fpdf:addPage>
</fpdf:pdf>
```

------------------------------------------------------------------------------------------------------------------------


### fpdf:setFont
Sets the font used to print character strings.

#### Parameter

| **Name**   | **Type** | **Default** | **Description**                                                       |
|:-----------|:---------|:------------|:----------------------------------------------------------------------|
| **family** | string   | Arial       | Family font.                                                          |
| **style**  | string   | N           | Font style. (N: regular, B: bold, I: italic, U: underline)            |
| **size**   | string   | 12          | Font size in points.                                                  |

#### Return
`void`

#### Usage:
```xml
<fpdf:setFont family="Arial" style="B" size="10.5" />
```

------------------------------------------------------------------------------------------------------------------------


### fpdf:setSourceFile
Set the source file for PDF page templates.

#### Parameter

| **Name** | **Type** | **Default**                                                         | **Description**                      |
|:---------|:---------|:--------------------------------------------------------------------|:-------------------------------------|
| **file** | string   | EXT:fluid_fpdf/Resources/Private/Pdf/TYPO3_2012-logo_sRGB_color.pdf | Filename with abs path of the source |

#### Return
`int` - Number of pages found in source file

#### Usage:
```xml
<fpdf:setSourceFile file="EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf" />
```

------------------------------------------------------------------------------------------------------------------------


### fpdf:useTemplate
Prints a PDF-Template into a page

#### Parameter

| **Name**           | **Type** | **Default** | **Description**                                                                       |
|:-------------------|:---------|:------------|:--------------------------------------------------------------------------------------|
| **pageNo**         | int      | 0           | The page number                                                                       |
| **x**              | int      | 0           | The x-position                                                                        |
| **y**              | int      | 0           | The y-position                                                                        |
| **w**              | int      | 0           | The new width of the template                                                         |
| **h**              | int      | 0           | The new height of the template                                                        |
| **adjustPageSize** | boolean  | false       | If set to true the current page will be resized to fit the dimensions of the template |

#### Return
`void`

#### Usage:
```xml
<fpdf:useTemplate pageNo="1" x="0" y="0" w="0" h="0" adjustPageSize="0" />
```

------------------------------------------------------------------------------------------------------------------------




## Frequently asked questions (FAQ)

### How to set an background PDF
Just use the *fpdf:useTemplate*-ViewHelper, for example:

```xml
<fpdf:pdf>
	<fpdf:addPage orientation="P" size="A4">
		<fpdf:setSourceFile file="EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf" />
		<fpdf:useTemplate pageNo="1" x="0" y="0" />	
		<f:comment>
			// Your PDF content...
		</f:comment>
	</fpdf:addPage>
</fpdf:pdf>
```


### How to use your own fonts in PDF
For using your own fonts in Fluid-FPDF, you need to convert the fonts into a special format.

The FPDF-Website supports a small Converting-Tool: http://www.fpdf.org/makefont/

![Fluid-FPDF Font-Converting Tool][#image_001]

Just choose your TTF-File and select the required encoding (for example ISO-8859-15). The result are two files - one
php-File and one z-File.

![Fluid-FPDF Font-Converting Result][#image_002]

For example, you want to convert the Ubunut-Font which is available on Google-Webfonts. You need the regular and bold
font. Downloaded you receive the following files:

*	Ubuntu-Regular.ttf
*	Ubuntu-Bold.ttf

Once converted, you have these files:

*	Ubuntu-Regular.php
*	Ubuntu-Regular.z
*	Ubuntu-Bold.php
*	Ubuntu-Bold.z

Follow the next steps, in order to use the font:

1.	Upload these two file into your TYPO3-Installation
2.	Define the new Fontpath in *fpdf:pdf*
3.	Add fonts with the *fpdf:addFont*
4.	Set the required font
5.	Write your text

```xml
<fpdf:pdf fontPath="EXT:theme_bootstrap4/Resources/Private/Fonts">
	<!-- Add required Fonts -->
	<fpdf:addFont family="Ubuntu" style="N" filename="Ubuntu-Regular.php" />
	<fpdf:addFont family="Ubuntu" style="B" filename="Ubuntu-Bold.php" />
	<fpdf:addPage orientation="P" size="A4">
		<!-- Set font -->
		<fpdf:setFont family="Ubuntu" style="N" size="12" />
		<!-- Write your text -->
		<fpdf:multiCell width="100" height="5" text="Hello world" />
		<f:comment>
			// Your PDF content...
		</f:comment>
	</fpdf:addPage>
</fpdf:pdf>
```

>	**Notice:**
>	
>	You need some support by converting your font? Get in contact with us: typo3@coding.ms


### A character of my font isn't displayed well in PDF (for example the €-character) - what to do?
First create a character dump of the string, in order to identify which characters are used:

```xml
<fpdf:debug.stringCharacterDump string="1.20 €" />
```

The result should look like this:

```text
1 : 49
. : 46
2 : 50
0 : 48
 : 32
? : 63
 : 0
```

Now you know, your €-character is placed by the character with ASCII-No. *63*.

In the next step, you perform a font dump with the font you want to use:

```xml
<fpdf:debug.fontDump />
```

I our example, the character on number *63* is displayed as *?* - that's not what we expect. Take a closer look on your
font dump and search for the €-character. We found it on ASCII-No. *162*.


### How to add a missing € character to a font

1.  Open Font with FontForge
2.  Menu -> View -> Goto -> search for 'Euro'
3.  Copy character 8364
4.  Select character 128 and paste the copied € character
5.  Menu -> Encoding -> Reencode -> ISO 8859-1
6.  Select character 128 -> right click on 'Glyph Info' -> enter 'Euro' in 'Glyph Name' and press 'Set From Name'
7. Menu -> File -> Generate Fonts -> save as .ttf

**Attention:** While generating the FPDF font with makefont, select *cp1250*!

Perform the same actions for adding a bullet:

1.  Menu -> View -> Goto -> search for 'Bullet'
2.  Copy character 8226
3.  Select character 149 and paste the copied character
4.  Select character 128 -> right click on 'Glyph Info' -> enter 'bullet' in 'Glyph Name' and press 'Set From Name'


## FAQ

### How to write a PDF template, which returns the PDF as string?
This is useful, if you like to attach the created PDF to a mail or something else.

```xml
<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true"><f:spaceless>
<f:format.raw>
	<fpdf:pdf destination="S">
		<fpdf:addPage orientation="P" size="A4">
			<fpdf:setFont family="Arial" style="B" size="10" />
			<fpdf:text x="10" y="10" text="Hello Pdf-World" />
			<fpdf:setXY x="40" y="40" />
			<fpdf:cell width="40" height="10" text="Hello World!" />
		</fpdf:addPage>
	</fpdf:pdf>
</f:format.raw>
</f:spaceless></html>
```

The important thing is, that there're no spaces and the PDF-Code is not escaped!

### How to pass some variables into the Footer-Template?

First you have to define the Footer-Template and pass the variables by the *footerArguments* Attribute.

```xml
<fpdf:pdf footerTemplate="{template.settings.footer.template}"
		  footerArguments="{template.settings}">

	<fpdf:addPage />

</fpdf:pdf>
```

Afterwards you're able to use it in Footer-Template.

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">

	<!-- Page numbers -->
	<fpdf:setFont family="Helvetica" style="{settings.footer.fontStyle}" size="{settings.footer.fontSize}" />

	<fpdf:setXY x="20" y="280" />
	<fpdf:cell width="170" text="{settings.footer.text}" align="L" border="T" height="{settings.footer.lineHeight}" />

	<fpdf:variable.set name="pages" value="Seite {fpdf:pageNo()} von |nb|" />
	<fpdf:cell text="{pages}" align="R" width="43" height="{settings.footer.lineHeight}" />

	<fpdf:setSourceFile file="{settings.backgroundPdf}" />
	<fpdf:useTemplate pageNo="1" x="0" y="0" />

</html>
```

In our example we have some TypoScript-Configuration, which we pass to the Fluid.

```typo3_typoscript
settings {
    backgroundPdf = EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf
    footer {
        template = EXT:crm/Resources/Private/Templates/Pdf/Footer.html
        text = PDF-Description
        pageNumberPrefix = Seite
        pageNumberDivider = von
        fontSize = 9
        fontStyle = N
        lineHeight = 6
    }
}
```



## Contribution
Every contribution is welcome! Hands on documentation, tutorials, Blog posts, Bug reporting, Bug fixing!

## Support


[#image_001]: https://cloud.coding.ms/files/fluid_fpdf/Documentation/Images/Fluid-FPDF-Font-Converting-Tool.png "Fluid-FPDF Font-Converting Tool"
[#image_002]: https://cloud.coding.ms/files/fluid_fpdf/Documentation/Images/Fluid-FPDF-Font-Converting-Result.png "Fluid-FPDF Font-Converting Result"
