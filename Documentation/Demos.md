# Fluid FPDF Demos


## FPDF Tutorial 1

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf>
	<fpdf:addPage>
		<fpdf:setFont family="Arial" style="B" size="16" />
		<fpdf:cell width="40" height="10" text="Hello World!" />
	</fpdf:addPage>
</fpdf:pdf>
</html>
```

Demo-Link in sidebar!



## FPDF Tutorial Bookmarks

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf>
	<fpdf:setFont family="Arial" style="" size="15" />

	<fpdf:addPage />
	<fpdf:bookmark title="Page 1" isUTF8="false" />
	<fpdf:bookmark title="Paragraph 1" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Paragraph 1" />
	<fpdf:ln height="50" />
	<fpdf:bookmark title="Paragraph 2" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Paragraph 2" />

	<fpdf:addPage />
	<fpdf:bookmark title="Page 2" isUTF8="false" />
	<fpdf:bookmark title="Paragraph 3" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Paragraph 3" />

</fpdf:pdf>
</html>
```

Demo-Link in sidebar!



## FPDF Tutorial Debugging

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf>

	<!-- show all border and debug information -->
	<fpdf:variable.set name="debug" value="1"/>
	<fpdf:variable.set name="debugText" value=""/>

	<fpdf:addPage>

		<!-- Automatic page break -->
		<fpdf:variable.set name="autoPageBreak" value="10"/>
		<fpdf:setAutoPageBreak margin="{autoPageBreak}"/>

		<!-- Set margins -->
		<fpdf:variable.set name="marginTop" value="32"/>
		<fpdf:variable.set name="marginLeft" value="9"/>
		<fpdf:variable.set name="marginRight" value="{fpdf:math.subtract(a: 210, b: 10)}"/>
		<fpdf:variable.set name="marginBottom" value="{fpdf:math.subtract(a: 297, b: autoPageBreak)}"/>
		<fpdf:variable.set name="marginLeftAndRight" value="20"/>
		<fpdf:variable.set name="marginTopAndBottom" value="{fpdf:math.sum(a: marginTop, b: autoPageBreak)}"/>
		<fpdf:setTopMargin margin="{marginTop}"/>
		<fpdf:setLeftMargin margin="{marginLeft}"/>
		<fpdf:setRightMargin margin="{marginRight}"/>

		<!-- Content width & height -->
		<fpdf:variable.set name="contentWidth" value="{fpdf:math.subtract(a: 210, b: marginLeftAndRight)}" />
		<fpdf:variable.set name="contentHeight" value="{fpdf:math.subtract(a: 297, b: marginTopAndBottom)}" />


		<!-- Print debug lines for margin -->
		<f:if condition="{debug}">
			<fpdf:setDrawColor hex="#FF0000"/>
			<fpdf:setLineWidth width="0.05"/>
			<!-- Margins -->
			<fpdf:line x1="{marginLeft}" x2="{marginLeft}" y1="0" y2="297.00"/>
			<fpdf:line x1="{marginRight}" x2="{marginRight}" y1="0" y2="297.00"/>
			<fpdf:line x1="0" x2="210.00" y1="{marginTop}" y2="{marginTop}"/>
			<fpdf:line x1="0" x2="210.00" y1="{marginBottom}" y2="{marginBottom}"/>
		</f:if>

<fpdf:variable.set name="debugText">{debugText}Before values:
x: {fpdf:getX()}
y: {fpdf:getY()}
</fpdf:variable.set>

		<fpdf:setXY x="{marginLeft}" y="{marginTop}" />
		<fpdf:setFont family="Arial" style="B" size="16" />
		<fpdf:cell width="40" height="10" text="Hello World!" border="{debug}" />

<fpdf:variable.set name="debugText">{debugText}After values:
x: {fpdf:getX()}
y: {fpdf:getY()}
</fpdf:variable.set>

		<!-- Print debugging output -->
		<f:if condition="{debug}">
			<fpdf:addPage />
			<fpdf:variable.set name="debugText">================================================================================
DEBUGGING:
================================================================================
{debugText}</fpdf:variable.set>
			<fpdf:setFont family="Courier" style="" size="10" />
			<fpdf:multiCell width="{contentWidth}" height="4" text="{debugText}" border="{debug}" />
		</f:if>

	</fpdf:addPage>
</fpdf:pdf>
</html>
```

Demo-Link in sidebar!



## FPDF Tutorial Index

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf>
	<fpdf:setFont family="Arial" style="" size="15" />

	<fpdf:addPage />
	<fpdf:bookmark title="Section 1" isUTF8="false" />
	<fpdf:cell width="0" height="6" text="Section 1" />
	<fpdf:ln height="10" />
	<fpdf:bookmark title="Subsection 1" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 1" />
	<fpdf:ln height="50" />
	<fpdf:bookmark title="Subsection 2" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 2" />

	<fpdf:addPage />
	<fpdf:bookmark title="Section 2" isUTF8="false" />
	<fpdf:cell width="0" height="6" text="Section 2" />
	<fpdf:ln height="10" />
	<fpdf:bookmark title="Subsection 3" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 3" />

	<fpdf:addPage />
	<fpdf:cell width="0" height="6" text="Index" />
	<fpdf:ln height="10" />
	<fpdf:createIndex />

</fpdf:pdf>
</html>
```

Demo-Link in sidebar!



## FPDF Tutorial Intro Index

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf>
	<fpdf:setFont family="Arial" style="" size="15" />

	<fpdf:addPage />
	<!-- Add an empty page -->

	<fpdf:addPage />
	<fpdf:bookmark title="Section 1" isUTF8="false" />
	<fpdf:cell width="0" height="6" text="Section 1" />
	<fpdf:ln height="10" />
	<fpdf:bookmark title="Subsection 1" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 1" />
	<fpdf:ln height="50" />
	<fpdf:bookmark title="Subsection 2" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 2" />

	<fpdf:addPage />
	<fpdf:bookmark title="Section 2" isUTF8="false" />
	<fpdf:cell width="0" height="6" text="Section 2" />
	<fpdf:ln height="10" />
	<fpdf:bookmark title="Subsection 3" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 3" />

	<fpdf:goToPage number="1" />
	<fpdf:setXY x="10" y="20" />
	<fpdf:cell width="0" height="6" text="Index" />
	<fpdf:ln height="10" />
	<fpdf:createIndex />

	<!-- !Go back to last page! -->
	<fpdf:goToPage />

</fpdf:pdf>
</html>

```

Demo-Link in sidebar!



## FPDF Tutorial Custom Index

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf>
	<fpdf:setFont family="Arial" style="" size="15" />

	<fpdf:addPage />
	<!-- Add an empty page -->

	<fpdf:addPage />
	<fpdf:bookmark title="Section 1" isUTF8="false" />
	<fpdf:cell width="0" height="6" text="Section 1" />
	<fpdf:ln height="10" />
	<fpdf:bookmark title="Subsection 1" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 1" />
	<fpdf:ln height="50" />
	<fpdf:bookmark title="Subsection 2" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 2" />

	<fpdf:addPage />
	<fpdf:bookmark title="Section 2" isUTF8="false" />
	<fpdf:cell width="0" height="6" text="Section 2" />
	<fpdf:ln height="10" />
	<fpdf:bookmark title="Subsection 3" isUTF8="false" level="1" y="-1" />
	<fpdf:cell width="0" height="6" text="Subsection 3" />

	<fpdf:goToPage number="1" />
	<fpdf:setXY x="10" y="20" />
	<fpdf:cell width="0" height="6" text="Index" />
	<fpdf:ln height="10" />

	<fpdf:variable.set name="indexEntries" value="{fpdf:getIndex()}" />
	<fpdf:variable.set name="indexStartX" value="30" />
	<fpdf:variable.set name="indexIndentX" value="10" />
	<f:if condition="{f:count(subject: indexEntries)}">
		<f:for each="{indexEntries}" as="indexEntry">
			<!-- Calculate x position -->
			<fpdf:variable.set name="indexEntryX" value="{fpdf:math.product(a: indexIndentX, b: indexEntry.l)}" />
			<fpdf:variable.set name="indexEntryX" value="{fpdf:math.sum(a: indexEntryX, b: indexStartX)}" />
			<!-- Calculate y position -->
			<fpdf:variable.set name="indexStartY" value="{fpdf:getY()}" />
			<fpdf:variable.set name="indexEntryY" value="{fpdf:math.sum(a: indexStartY, b: 10)}" />
			<!-- Print entry -->
			<fpdf:setXY x="{indexEntryX}" y="{indexEntryY}" />
			<fpdf:cell text="{indexEntry.t} (Seite {indexEntry.p})" align="L" width="120" />
		</f:for>
	</f:if>

	<!-- ATTENTION: Don't forget to go back to last page!!! -->
	<fpdf:goToPage />

</fpdf:pdf>
</html>
```

Demo-Link in sidebar!



## FPDF Tutorial Footer

In this tutorial we want to reuse a footer template on each PDF page. For that you need to create an additional Template, we call it simply `Templates/Pdf/Footer.html`. This template could look like this:

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">

	<!-- Page numbers -->
	<fpdf:setXY x="160" y="280" />
	<fpdf:cell text="{fpdf:pageNo()}/|nb|" align="R" width="40" />

</html>
```

Additionally we need a FPDF template like usual. In this template we need to define the path to the footer template in the `<fpdf:pdf` tag. Our testing template looks like this:

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
	  xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	  xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
	  data-namespace-typo3-fluid="true">
<fpdf:pdf footerTemplate="EXT:fluid_fpdf/Resources/Private/Templates/Pdf/Footer.html">
	<fpdf:setFont family="Arial" style="" size="15" />

	<fpdf:addPage />

	<fpdf:addPage />

	<fpdf:addPage />

	<fpdf:addPage />

</fpdf:pdf>
</html>
```

Demo-Link in sidebar!



## FPDI Example

```xml
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"
      xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
      xmlns:fpdf="http://typo3.org/ns/CodingMs/FluidFpdf/ViewHelpers"
      data-namespace-typo3-fluid="true">
<fpdf:pdf>
    <fpdf:addPage orientation="P" size="A4">
        <fpdf:setSourceFile file="EXT:fluid_fpdf/Resources/Private/Pdf/TYPO3_2012-logo_sRGB_color.pdf" />
        <fpdf:useTemplate pageNo="1" x="0" y="0" />
        <fpdf:setFont family="Arial" style="B" size="10" />
        <fpdf:text x="10" y="10" text="Hello Pdf-World" />
        <fpdf:setXY x="40" y="40" />
        <fpdf:cell width="40" height="10" text="Hello World!" />
    </fpdf:addPage>
</fpdf:pdf>
</html>
```

Demo-Link in sidebar!