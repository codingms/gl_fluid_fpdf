<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Prints a character string. The origin is on the left of the first character, on the baseline.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class TextViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('text', 'string', 'String to print.', false, '');
        $this->registerArgument('x', 'float', 'Abscissa of the origin.', false, 0.0);
        $this->registerArgument('y', 'float', 'Ordinate of the origin.', false, 0.0);
    }

    /**
     * Prints a character string. The origin is on the left of the first character, on the baseline
     *
     * @return void
     */
    public function render()
    {
        $text = html_entity_decode($this->arguments['text']);
        // Map characters
        if ($this->templateVariableContainer->exists('characterMap')) {
            $characterMap = $this->templateVariableContainer->get('characterMap');
            if (is_array($characterMap) && !empty($characterMap)) {
                foreach ($characterMap as $asciiNo => $character) {
                    $text = str_replace($character, utf8_encode(chr($asciiNo)), $text);
                }
            }
        }
        if (iconv("UTF-8", "cp1252//TRANSLIT", $text) != $text) {
            $text = iconv("UTF-8", "cp1252//TRANSLIT", $text);
        } else {
            $text = utf8_decode($text);
        }
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->Text($this->arguments['x'], $this->arguments['y'], $text);
        $this->renderChildren();
    }

}