<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the page width
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetPageWidthViewHelper extends AbstractViewHelper
{

    /**
     * Returns the page width
     *
     * @return  integer
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $pageWidth = $fpdf->GetPageWidth();
        $this->renderChildren();
        return $pageWidth;
    }

}
