<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the abscissa of the current position.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetXViewHelper extends AbstractViewHelper
{

    /**
     * Returns the abscissa of the current position.
     *
     * @return  integer
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $x = $fpdf->GetX();
        $this->renderChildren();
        return $x;
    }

}
