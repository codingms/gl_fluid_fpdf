<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Activates or deactivates page compression. When activated, the internal
 * representation of each page is compressed, which leads to a compression
 * ratio of about 2 for the resulting document.
 * Compression is on by default.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetCompressionViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('compress', 'bool', 'Boolean indicating if compression must be enabled.', false, true);
    }

    /**
     * Activates or deactivates page compression. When activated, the internal
     * representation of each page is compressed, which leads to a compression
     * ratio of about 2 for the resulting document.
     * Compression is on by default.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetCompression($this->arguments['compress']);
        $this->renderChildren();
    }

}
