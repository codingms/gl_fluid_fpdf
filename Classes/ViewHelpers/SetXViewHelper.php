<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines the abscissa of the current position. If the passed value is negative, it is relative to the right of the page.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetXViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('x', 'float', 'The value of the abscissa.', false, 0.0);
    }

    /**
     * Defines the abscissa of the current position. If the passed value is negative, it is relative to the right of the page.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetX($this->arguments['x']);
        $this->renderChildren();
    }

}
