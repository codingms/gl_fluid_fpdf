<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the current page number.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class PageNoViewHelper extends AbstractViewHelper
{

    /**
     * Returns the current page number.
     *
     * @return integer
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $pageNo = $fpdf->PageNo();
        $this->renderChildren();
        return $pageNo;
    }

}
