<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the length of a string in user unit. A font must be selected.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetStringWidthViewHelper extends AbstractViewHelper
{

    /**
     * Returns the length of a string in user unit. A font must be selected.
     *
     * @param string $string The string whose length is to be computed.
     * @return  integer
     */
    public function render($string = '')
    {
        $string = utf8_decode($string);
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $stringWidth = $fpdf->GetStringWidth($string);
        $this->renderChildren();
        return $stringWidth;
    }

}
