<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Condition;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class IsValidImageViewHelper extends AbstractConditionViewHelper
{

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('publicUrl', 'string', 'Image url', true);
        $this->registerArgument('vector', 'boolean', 'Is vector image format', false, false);
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending ViewHelpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexibility in overriding this method.
     * @return bool
     */
    static protected function evaluateCondition($arguments = null)
    {
        $pathInfo = pathinfo(strtolower($arguments['publicUrl']));
        $validExtensions = ['jpg', 'jpeg', 'png', 'gif'];
        if (true === $arguments['vector']) {
            $validExtensions = ['eps', 'ai'];
        }
        return in_array($pathInfo['extension'], $validExtensions);
    }

}