<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Debug;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Writes a character dump of a string
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class StringCharacterDumpViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('string', 'string', 'String which should be dumped', false, '');
    }

    /**
     * Writes a character dump of a string
     *
     * @return void
     */
    public function render()
    {
        $string = $this->arguments['string'];
        $debug = false;
        if (iconv("UTF-8", "cp1252//TRANSLIT", $string) != $string) {
            $string = iconv("UTF-8", "cp1252//TRANSLIT", $string);
        } else {
            $string = utf8_decode($string);
        }
        if ($debug) {
            echo "<pre>";
        }
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        for ($i = 0; $i <= strlen($string); $i++) {
            if ($debug) {
                echo $string[$i] . ':' . ord($string[$i]) . "<br />";
            }
            $fpdf->Cell(12, 5.5, $string[$i] . ' : ');
            $fpdf->Cell(0, 5.5, ord($string[$i]), 0, 1);
        }
        if ($debug) {
            exit;
        }
    }

}
