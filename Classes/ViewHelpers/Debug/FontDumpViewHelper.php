<?php

namespace CodingMs\FluidFpdf\ViewHelpers\Debug;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Writes a font dump
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class FontDumpViewHelper extends AbstractViewHelper
{

    /**
     * Writes a font dump
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        for ($i = 32; $i <= 255; $i++) {
            $fpdf->Cell(12, 5.5, $i . ' : ');
            $fpdf->Cell(0, 5.5, chr($i), 0, 1);
        }
    }

}
