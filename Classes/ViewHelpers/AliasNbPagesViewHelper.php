<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines an alias for the total number of pages. It will be substituted as the document is closed.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class AliasNbPagesViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('alias', 'string', 'The alias. Default value: |nb|.', false, '|nb|');
    }

    /**
     * Defines an alias for the total number of pages. It will be substituted as the document is closed.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->AliasNbPages($this->arguments['alias']);
        $this->renderChildren();
    }

}
