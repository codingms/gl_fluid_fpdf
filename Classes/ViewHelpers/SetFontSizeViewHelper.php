<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines the size of the current font.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetFontSizeViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('size', 'float', 'The size (in points).', false, 0.0);
    }

    /**
     * Defines the size of the current font.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetFontSize($this->arguments['size']);
        $this->renderChildren();
    }

}
