<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Performs a line break. The current abscissa goes back to the left margin
 * and the ordinate increases by the amount passed in parameter.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class LnViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('height', 'mixed', 'The height of the break. By default, the value equals the height of the last printed cell.', false, null);
    }

    /**
     * Performs a line break. The current abscissa goes back to the left margin
     * and the ordinate increases by the amount passed in parameter.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        if ($this->arguments['height'] !== null) {
            $fpdf->Ln($this->arguments['height']);
        } else {
            $fpdf->Ln();
        }
        $this->renderChildren();
    }

}