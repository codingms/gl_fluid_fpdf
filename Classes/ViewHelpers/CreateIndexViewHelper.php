<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Adds a index in PDF
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class CreateIndexViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('pagePrefix', 'string', 'The prefix for the page numbers', false, 'page ');
    }

    /**
     * Adds a index in PDF
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->CreateIndex($this->arguments['pagePrefix']);
        $this->renderChildren();
    }

}
