<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the index
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetIndexViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('level', 'int', 'The level of index entries. -1 means all levels.', false, -1);
    }

    /**
     * Returns the index
     *
     * @return array
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $index = $fpdf->GetIndex($this->arguments['level']);
        $this->renderChildren();
        return $index;
    }

}
