<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Send the document to a given destination: browser, file or string.
 * In the case of browser, the plug-in may be used (if present) or a
 * download ("Save as" dialog box) may be forced.
 * The method first calls Close() if necessary to terminate the document.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class OutputViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('filename', 'string', 'The name of the file', false, 'FluidFpdf.pdf');
        $this->registerArgument('destination', 'string', 'Destination where to send the document. It can take one of the following values: I, D, F, S', false, 'I');
    }

    /**
     * Send the document to a given destination: browser, file or string.
     * In the case of browser, the plug-in may be used (if present) or a
     * download ("Save as" dialog box) may be forced.
     * The method first calls Close() if necessary to terminate the document.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->Output($this->arguments['filename'], $this->arguments['destination']);
        $this->renderChildren();
    }

}