<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the image height based on a giving width
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetImageHeightFromWidthViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('image', FileReference::class, 'Image file reference.');
        $this->registerArgument('width', 'int', 'Width of the target image');
    }

    /**
     * Returns the image height based on a giving width
     *
     * @return string
     */
    public function render()
    {
        /** @var FileReference $image */
        $image = $this->arguments['image'];
        $file = $image->getOriginalResource();
        $filePublicUrl = $file->getPublicUrl();
        $filePath = GeneralUtility::getFileAbsFileName($filePublicUrl);
        $return = 0;
        if (file_exists($filePath)) {
            list($originalWidth, $originalHeight) = getimagesize($filePath);
            $onePercent = $originalWidth / 100;
            $widthPercent = $this->arguments['width'] / $onePercent;
            $return = $originalHeight / 100 * $widthPercent;
        }
        return $return;
    }

}
