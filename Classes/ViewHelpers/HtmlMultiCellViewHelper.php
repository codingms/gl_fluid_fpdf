<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Prints a HTML text
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class HtmlMultiCellViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('text', 'string', 'String to print.', false, '');
        $this->registerArgument('x', 'float', 'X position of the text block', false, 0.0);
        $this->registerArgument('width', 'float', 'Width of cells. If 0, they extend up to the right margin of the page.', false, 0.0);
        $this->registerArgument('height', 'float', 'Height of a line.', false, 0.0);
        $this->registerArgument('listIndent', 'float', 'Indent of listings and enumerations', false, 8.0);
        $this->registerArgument('paragraphMarginBottom', 'float', 'Margin bottom for paragraphs, listings and enumerations.', false, 0.0);
    }

    /**
     * Prints a HTML text
     *
     * @return void
     */
    public function render()
    {
        $text = html_entity_decode($this->arguments['text']);
        // Map characters
        if ($this->templateVariableContainer->exists('characterMap')) {
            $characterMap = $this->templateVariableContainer->get('characterMap');
            if (is_array($characterMap) && !empty($characterMap)) {
                foreach ($characterMap as $asciiNo => $character) {
                    $text = str_replace($character, utf8_encode(chr($asciiNo)), $text);
                }
            }
        }
        if (iconv("UTF-8", "cp1252//TRANSLIT", $text) != $text) {
            $text = iconv("UTF-8", "cp1252//TRANSLIT", $text);
        } else {
            $text = utf8_decode($text);
        }
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->WriteHTML($text, $this->arguments['x'], $this->arguments['width'], $this->arguments['height'], $this->arguments['listIndent'], $this->arguments['paragraphMarginBottom']);
        $this->renderChildren();
    }

}