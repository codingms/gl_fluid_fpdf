<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

/**
 * Returns the ordinate of the current position.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetYViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * Returns the ordinate of the current position.
     *
     * @return  integer
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $y = $fpdf->GetY();
        $this->renderChildren();
        return $y;
    }

}
