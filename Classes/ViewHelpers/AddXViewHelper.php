<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Adds a value to the current X
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class AddXViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('add', 'float', 'The value to add', false, 0.0);
    }

    /**
     * Adds a value to the current X
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetX($fpdf->getX() + $this->arguments['add']);
        $this->renderChildren();
    }

}
