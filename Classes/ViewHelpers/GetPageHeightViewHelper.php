<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns the page height
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GetPageHeightViewHelper extends AbstractViewHelper
{

    /**
     *  Returns the page height
     *
     * @return  integer
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $pageHeight = $fpdf->GetPageHeight();
        $this->renderChildren();
        return $pageHeight;
    }

}
