<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines the font/letter spacing.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetFontSpacingViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('spacing', 'float', 'Letter spacing.', false, 0.0);
    }

    /**
     * Defines the font/letter spacing.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetFontSpacing($this->arguments['spacing']);
        $this->renderChildren();
    }

}