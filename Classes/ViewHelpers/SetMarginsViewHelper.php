<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines the left, top and right margins. By default, they equal 1 cm.
 * Call this method to change them.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetMarginsViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('margin', 'float', 'Left margin.', false, 0.0);
        $this->registerArgument('margin', 'float', 'Top margin.', false, 0.0);
        $this->registerArgument('margin', 'float', 'Right margin. Default value is the left one.', false, 0.0);
    }

    /**
     * Defines the left, top and right margins. By default, they equal 1 cm.
     * Call this method to change them.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetMargins($this->arguments['left'], $this->arguments['top'], $this->arguments['right']);
        $this->renderChildren();
    }

}