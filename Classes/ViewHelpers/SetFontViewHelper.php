<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Sets the font used to print character strings.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetFontViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('family', 'string', 'Family font.', false, 'Arial');
        $this->registerArgument('style', 'string', 'Font style. (N: regular, B: bold, I: italic, U: underline)', false, 'N');
        $this->registerArgument('size', 'string', 'Font size in points.', false, '12');
    }

    /**
     * @return void
     */
    public function render()
    {
        if (trim(strtoupper($this->arguments['style'])) === 'N') {
            $this->arguments['style'] = '';
        }
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetFont($this->arguments['family'], $this->arguments['style'], $this->arguments['size']);
        $this->renderChildren();
    }

}
