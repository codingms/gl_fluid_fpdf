<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines the page and position a link points to.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetLinkViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('link', 'string', 'The link identifier returned by AddLink().', false, '');
        $this->registerArgument('y', 'float', 'Ordinate of target position; -1 indicates the current position. The default value is 0 (top of page).', false, 0.0);
        $this->registerArgument('p', 'int', 'Number of target page; -1 indicates the current page. This is the default value.', false, 0);
    }

    /**
     * Defines the page and position a link points to.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetLink($this->arguments['link'], $this->arguments['y'], $this->arguments['page']);
        $this->renderChildren();
    }

}
