<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Set the source file for PDF page templates
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetSourceFileViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('file', 'string', 'Filename with abs path of the source', false, 'EXT:fluid_fpdf/Resources/Private/Pdf/TYPO3_2012-logo_sRGB_color.pdf');
    }

    /**
     * Set the source file for PDF page templates
     *
     * @return  int Number of pages found in source file
     * @throws \Exception
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $absFile = GeneralUtility::getFileAbsFileName($this->arguments['file']);
        if ($absFile === NULL) {
            throw new \Exception('PDF-Template \'' . $absFile . '\' not found');
        }
        // set the source file
        $numberOfPagesInSourceFile = $fpdf->setSourceFile($absFile);
        $this->renderChildren();
        return $numberOfPagesInSourceFile;
    }

}
