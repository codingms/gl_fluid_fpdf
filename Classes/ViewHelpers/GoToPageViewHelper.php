<?php

namespace CodingMs\FluidFpdf\ViewHelpers;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Got to page in PDF
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class GoToPageViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('number', 'int', 'Number of the page. -1 means the last page.', false, -1);
    }

    /**
     * Got to page in PDF
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->GoToPage($this->arguments['number']);
        $this->renderChildren();
    }

}
