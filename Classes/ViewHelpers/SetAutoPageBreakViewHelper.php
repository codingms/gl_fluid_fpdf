<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Enables or disables the automatic page breaking mode.
 * When enabling, the second parameter is the distance from
 * the bottom of the page that defines the triggering limit.
 * By default, the mode is on and the margin is 2 cm.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetAutoPageBreakViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('auto', 'bool', 'Boolean indicating if mode should be on or off.', false, true);
        $this->registerArgument('margin', 'float', 'Distance from the bottom of the page.', false, 20.0);
    }

    /**
     * Enables or disables the automatic page breaking mode.
     * When enabling, the second parameter is the distance from
     * the bottom of the page that defines the triggering limit.
     * By default, the mode is on and the margin is 2 cm.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetAutoPageBreak($this->arguments['auto'], $this->arguments['margin']);
        $this->renderChildren();
    }

}
