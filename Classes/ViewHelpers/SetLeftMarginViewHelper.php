<?php

namespace CodingMs\FluidFpdf\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Defines the left margin. The method can be called before creating the first page.
 * If the current abscissa gets out of page, it is brought back to the margin.
 *
 * @package TYPO3
 * @subpackage fluid_fpdf
 */
class SetLeftMarginViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('margin', 'float', 'The margin.', false, 0.0);
    }

    /**
     * Defines the left margin. The method can be called before creating the first page.
     * If the current abscissa gets out of page, it is brought back to the margin.
     *
     * @return void
     */
    public function render()
    {
        /** @var \FluidFpdf $fpdf */
        $fpdf = $this->templateVariableContainer->get('fpdf');
        $fpdf->SetLeftMargin($this->arguments['margin']);
        $this->renderChildren();
    }

}
