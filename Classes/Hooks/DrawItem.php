<?php

namespace CodingMs\FluidFpdf\Hooks;

use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * Class/Function which manipulates the rendering of item example content
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @package fluid_fpdf
 */
class DrawItem implements PageLayoutViewDrawItemHookInterface
{

    /**
     * @var \TYPO3\CMS\Lang\LanguageService
     */
    var $lang;

    public function __construct()
    {
        $this->lang = GeneralUtility::makeInstance(LanguageService::class);
        $this->lang->init($GLOBALS['BE_USER']->uc['lang']);
    }

    /**
     * Processes the item to be rendered before the actual example content gets rendered
     * Deactivates the original example content output
     *
     * @param PageLayoutView $parentObject : The parent object that triggered this hook
     * @param boolean $drawItem : A switch to tell the parent object, if the item still must be drawn
     * @param string $headerContent : The content of the item header
     * @param string $itemContent : The content of the item itself
     * @param array $row : The current data row for this item
     *
     * @return    void
     */
    public function preProcess(PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row)
    {
        if ($row['CType'] == 'list' && $row['list_type'] == 'fluidfpdf_pdf') {
            $drawItem = FALSE;
            $lllFile = 'LLL:EXT:fluid_fpdf/Resources/Private/Language/locallang_db.xlf:';
            // Get general tab data
            if (!empty($row['pi_flexform'])) {
                $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);
                $flexformArray = $flexFormService->convertFlexFormContentToArray($row['pi_flexform']);
                $itemContent .= '<table class="table table-bordered table-striped">';
                if (trim($row['header']) !== '') {
                    $itemContent .= $this->getTableRow(
                        'Title',
                        $row['header']
                    );
                }
                if (trim($flexformArray['settings']['template']) !== '') {
                    $itemContent .= $this->getTableRow(
                        $this->lang->sL($lllFile . 'tx_fluidfpdf_flexform_pdf.template'),
                        $flexformArray['settings']['template']
                    );
                }
                $itemContent .= '</table>';
            }
        }
    }

    /**
     * @param $label
     * @param $value
     * @return string
     */
    protected function getTableRow($label, $value)
    {
        $itemContent = '';
        $itemContent .= '<tr>';
        $itemContent .= '<th>' . $label . '</th>';
        if (is_array($value)) {
            $itemContent .= '<td>' . implode(', ', $value) . '</td>';
        } else {
            $itemContent .= '<td>' . $value . '</td>';
        }
        $itemContent .= '</tr>';
        return $itemContent;
    }

}
