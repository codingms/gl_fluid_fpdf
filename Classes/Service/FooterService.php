<?php

namespace CodingMs\FluidFpdf\Service;

use \TYPO3\CMS\Fluid\View\StandaloneView;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Services for printing PDF footer
 *
 * @package fluid_pdf
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class FooterService
{

    /**
     * Creates a footer
     * @param \FluidFpdf
     * @return void
     */
    public function write(\FluidFpdf $fpdf)
    {
        $pdfView = new StandaloneView();
        $templateRootPath = GeneralUtility::getFileAbsFileName($fpdf->GetFooterTemplate());
        if (file_exists($templateRootPath)) {
            $pdfView->setTemplatePathAndFilename($templateRootPath);
            $pdfView->assign('fpdf', $fpdf);
            $pdfView->assign('settings', $fpdf->getFooterArguments());
            $pdfView->render();
        }
    }

}
