# Fluid-FPDF Change-Log


## Release of version 2.0.1

### 2019-01-28  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing argument definition in SetFontSize ViewHelper.



## Release of version 2.0.0

### 2019-01-20  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Migration for TYPO3 9.5.

### 2019-01-08  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding paragraphMarginBottom attribute in HtmlMultiCell-ViewHelper.

### 2018-12-07  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Removing double UTF8 encoding in Cell-ViewHelper.

### 2018-09-14  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Optimizing spaces between ol/ul li in HtmlMultiCell.



## 2018-08-23  Release of version 1.13.0

### 2018-08-23  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding HtmlMultiCell ViewHelper, which is able to handle the following HTML tags: <b><u><i><a><p><br><strike><strong><small><em><ul><ol><li>.
*	[BUGFIX] Fixing conposer.json.

### 2018-06-05  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding UTF8 Encode ViewHelper.
*	[BUGFIX] Fixing encoding issue in Cell ViewHelper.



## 2018-06-04  Release of version 1.12.0

### 2018-06-04  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding possibility to pass variables into Footer-Template (see Readme.md).



## 2018-02-22  Release of version 1.11.1

### 2018-02-21  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing image path issue by using config.absRefPrefix = /.



## 2018-02-02  Release of version 1.11.0

### 2018-02-01  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing image path issue by using config.absRefPrefix = /.

### 2018-01-18  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding IsValidImage-ViewHelper for checking a valid image type. It check if an image is vector based and if the image owns a supported file extensions.

### 2018-01-15  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing UseTemplate-ViewHelper, which import the first page twice.
*	[TASK] Updating the FPDI library to version 2.0.0.



## 2018-01-13  Release of version 1.10.1

### 2018-01-13  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing AddFont-ViewHelper.



## 2018-01-11  Release of version 1.10.0

### 2018-01-11  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding support for EPS and AI image files.



### 2017-12-22  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding custom sizes for pages. Instead of using A4, just use 213x321 in mm.



## 2017-11-23  Release of version 1.9.0

### 2017-11-23  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Get template container variable optimization
*	[TASK] Adding new demo in documentation

### 2017-11-22  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding ViewHelper for GetImageHeightFromWidth and GetImageWidthFromHeight



## 2017-11-19  Release of version 1.8.0

### 2017-11-16  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding UTF8-Decode ViewHelper



## 2017-10-24  Release of version 1.7.0

### 2017-10-24  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing image ViewHelper. By using config.absRefPrefix the image path is broken.

### 2017-10-20  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding Footer feature

### 2017-09-29  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing ViewHelper attribute inline line breaks



## 2017-09-27  Release of version 1.6.0

*	[FEATURE] Adding GetImageOrientation ViewHelper for identifying, if an image is portrait, landscape or squared.
*	[FEATURE] Adding SetFontSpacing ViewHelper for setting letter spacing.
*	[BUGFIX] Fixing pageno ViewHelper
*	[FEATURE] Adding GetIndexOrientation ViewHelper for writing a customized index.



## 2017-09-25  Release of version 1.5.0

*	[BUGFIX] Fixing page module content preview

### 2017-09-23  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding Bookmarks feature (see: http://fpdf.org/en/script/script1.php).
*	[FEATURE] Adding Index feature (see: http://fpdf.org/en/script/script13.php).
*	[FEATURE] Adding GoToPage-ViewHelper for getting to previous pages (see: FpdfTutorialIntroIndex.html example).




### 2017-08-28  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing encoding of windows quotes

### 2017-07-11  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing style (N === '') in SetFont PDF-ViewHelper



## 2017-07-09  Release of version 1.4.2

### 2017-07-09  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Support for destination S in PDF-ViewHelper

### 2017-06-25  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing exit in PDF-ViewHelper



## 2017-06-15  Release of version 1.4.1

### 2017-06-15  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Fixing version numbers



## 2017-06-14  Release of version 1.4.0

### 2017-06-14  Thomas Deuling  <typo3@coding.ms>

*	[TASK] Migration to TYPO3 8.6



## 2017-04-11  Release of version 1.3.0

### 2017-02-17  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding Debug/FontDump-ViewHelper for dumping all characters with ASCII-No.
*	[FEATURE] Adding Debug/StringCharacterDump-ViewHelper for dumping all characters from a string (with ASCII-No.)
*	[FEATURE] PDF-ViewHelper now got a *characterMap* attributes, which allows you to map characters to other ASCII-No. within the PDF-Font.

### 2017-02-16  Thomas Deuling  <typo3@coding.ms>

*	[TASK] SetFont-ViewHelper: Attribute style: Default value is now 'N'
*	[TASK] UseTemplate-ViewHelper: Attribute pageNo: Default value is now '0'
*	[TASK] Documentation
*	[BUGFIX] Fixing GetStringWidth ViewHelper



## 2015-09-02  Release of version 1.2.1

### 2015-09-19  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing conversion of the euro symbol



## 2015-09-02  Release of version 1.2.0

### 2015-09-02  Thomas Deuling  <typo3@coding.ms>

*	[TASK] PHP 7 compatibility
*	[FEATURE] Adding GetPageWidth- and GetPageHeight-ViewHelper
*	[TASK] Upgrade to FPDF 1.8.1

### 2015-08-29  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding hex color parameter for SetDrawColor-, SetFillColor- and SetTextColor-ViewHelper



## 2015-05-19  Release of version 1.1.0

### 2015-05-19  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding FPDI feature, merge existing PDF-Pages to PDF